function adaptViewTilt(varargin)
%%
%cd S:\matlab\adaptViewTilt

%%
importCADasciiFile = false; % false = read mat file
calibrateCamera    = false; % false = load calib from file
testFaceDetection  = false; % false = omit face detection test phase
%%
filename = 'McLaren720sAscii.stl'; % a simple demo part
filename = 'MoonLampSimplified.ascii.stl';
fileGiven = false;
if(exist('varargin','var')) % nargin == 0  
    if(length(varargin)>0)
        filename = varargin{1}
        fileGiven = true;
    end
end
if ~fileGiven
    warning(['No file specified, using demo file: ' filename]);
end
clear fileGiven
%
%% Read the CAD data file:
if(importCADasciiFile)
    [F, V, C] = CADstlread(filename);
else
    %load('McLaren720s.mat','C','F','V');
    load('MoonLampSimplified.mat','C','F','V');
    %load('3dFaceReducedMax.mat','C','F','V');
end
%%
hFig3D = figure;
  p = patch('faces', F, 'vertices' ,V);
    %set(p, 'facec', 'b');              % Set the face color (force it)
    set(p, 'facec', 'flat');            % Set the face color flat
    set(p, 'FaceVertexCData', C);       % Set the color (from file)
    %set(p, 'facealpha',.4)             % Use for transparency
    set(p, 'EdgeColor','none');         % Set the edge color
    %set(p, 'EdgeColor',[1 0 0 ]);      % Use to see triangles, if needed.
    light                               % add a default light
    daspect([1 1 1])                    % Setting the aspect ratio
    view(3)                             % Isometric view
    xlabel('X'),ylabel('Y'),zlabel('Z')
    drawnow                             %, axis manual
    %
    colormap gray(256);

V2 = V-min(min(V));
V2 = 2.*V2./max(max(V2))-1;
set(p,'Vertices',V2)
view(90,0)

%%
% campos([11.8743 -0.5 -0.2])
ocp = campos;
% camtarget([0   -0.5000   -0.2000]);
oct = camtarget;

%% connect the webcam and set up FaceDetector
clear cam;
camnum = 1;
cam = webcam(camnum);
%cam.set('Resolution','160x120'); %
cam.set('Resolution','640x480'); % default
vid=snapshot(cam);
camsiz = size(vid); camsiz=camsiz([2 1]);
FaceDetector = vision.CascadeObjectDetector('MinSize',[35 35].*(camsiz(1)/160));
% FaceDetector = vision.CascadeObjectDetector();

%% calibrate the webcam
if(calibrateCamera)
nf=figure('Position',[0 0 1920 1200],'menubar','none','toolbar','none');
set(nf,'DefaultAxesPosition',[0 0 1 1]);
vid = rgb2gray(cam.snapshot);
hImg = image(0.*vid+208);
hold on;
plot(size(vid,2)/2,size(vid,1)/2,'kx','MarkerSize',40);
colormap(gray(256));
hRect = rectangle('position', [1 1 1 1], 'lineWidth', 2, 'edgeColor', 'y');

numCalib = 100;
hp = zeros(numCalib,2)-1;
area = zeros(numCalib,1)-1;

for ii=1:numCalib % loop to continuously detect the face
    
    vid=snapshot(cam);  %get a snapshot of webcam
    vid = rgb2gray(vid);    %convert to grayscale
    % img = flip(vid, 2); % Flips the image horizontally
    % set(hImg,'CData',0.*img+208);
    
    bbox = step(FaceDetector, vid); % Creating bounding box using detector 

    if ~ isempty(bbox)  %if face exists 
        biggest_box=1;     
        for i=1:rank(bbox) %find the biggest face
         if bbox(i,3)>bbox(biggest_box,3)
             biggest_box=i;
         end
        end
        % for i=1:size(bbox,1)    %draw all the regions that contain face
        for i=biggest_box    %draw regions around indentified face
            set(hRect,'position', bbox(i, :));
        end

        faceImage = imcrop(vid,bbox(biggest_box,:)); % extract the face from the image
        faceOffset = bbox(biggest_box,1:2)-1;
        headPos = faceOffset + bbox(biggest_box,3:4)/2;
        hp(ii,:) = headPos;
        area(ii,:) = prod(bbox(biggest_box,3:4));
    end 
end

delete(nf)
goodMeasurements = area>0;
area = area(goodMeasurements,:);
hp   = hp(goodMeasurements,:);
restArea = median(area);
restHP   = median(hp);

else
    load('headPosCalibration.mat')
    restArea = restArea./(640*480).*prod(camsiz)./0.5;
    restHP = restHP./[640 480].*camsiz;
end % calibrateCamera

%% check correct detection of faces
if(testFaceDetection)
numFaceDetection = 500;
vid=snapshot(cam);      %get a snapshot of webcam
vid = rgb2gray(vid);    %convert to grayscale
tmpfig = figure;
htmpImg = image(vid);
colormap(gray);
htmpText = text(camsiz(1)/2,camsiz(2)/2,'No face detected','HorizontalAlignment','center','FontSize',24,'Color',[1 0 0]);
htmpRect = rectangle('position', [1 1 1 1], 'lineWidth', 2, 'edgeColor', 'y');
for k=1:numFaceDetection % loop to continuously detect the face
    vid=snapshot(cam);  %get a snapshot of webcam
    vid = rgb2gray(vid);    %convert to grayscale
    % img = flip(vid, 2); % Flips the image horizontally
    set(htmpImg,'CData',vid);
    bbox = step(FaceDetector, vid); % Creating bounding box using detector 
    if ~ isempty(bbox)  %if face exists 
        biggest_box=1;     
        for i=1:rank(bbox) %find the biggest face
         if bbox(i,3)>bbox(biggest_box,3)
             biggest_box=i;
         end
        end
        set(htmpText,'Visible','off');
        for i=biggest_box    %draw regions around indentified face
            set(htmpRect,'position', bbox(i, :));
        end
    else
        set(htmpText,'Visible','on');
        set(htmpRect,'position', [1 1 1 1]);
    end
end
end % testFaceDetection

%%
% hFig3D = gcf;
figure(hFig3D);
scrSize = get(0,'ScreenSize');
set(hFig3D,'Position',[0 0 1920 1200],'menubar','none','toolbar','none');
set(hFig3D,'Position',[0 0 scrSize(3:4)],'menubar','none','toolbar','none');
%set(hFig3D,'Position',[680  630   560   420],'menubar','none','toolbar','none');
axis off
li = findobj(gca,'Type','Light');
ohl = findobj(li,'Tag','CamLight');
delete(li);
delete(ohl);
hlight = camlight('headlight');set(hlight,'Tag','CamLight');
areaRingBuffer = 1;
areaRingIndex  = 0;
FILT = 0;% zeros(size(areaRingBuffer));
FF = 1/4;
tpf = zeros(100,4);
tpfi = 0;
einmalDurch = false;
endlessMode = true;
minRuns = 200;
while(~einmalDurch) % loop to continuously detect the face
    tic;
    tpfi=mod(tpfi,minRuns)+1;
    vid=snapshot(cam);  %get a snapshot of webcam
    vid = rgb2gray(vid);    %convert to grayscale
    % img = flip(vid, 2); % Flips the image horizontally
    
    bbox = step(FaceDetector, vid); % Creating bounding box using detector 
    tpf(tpfi,1:4)=toc;    % face detector

    if ~ isempty(bbox)  %if face exists 
        biggest_box=1;     
        for i=1:rank(bbox) %find the biggest face
         if bbox(i,3)>bbox(biggest_box,3)
             biggest_box=i;
         end
        end

        faceOffset = bbox(biggest_box,1:2)-1;
        headPos = faceOffset + bbox(biggest_box,3:4)/2;
        headPos = (headPos - restHP).*[-1 1].*[640 480]./camsiz;
        areaRatio = -(prod(bbox(biggest_box,3:4))-restArea)./restArea;
        % fprintf(1,' %g %g\n',headPos);
    tpf(tpfi,2)=toc;    % position calculation
        campos(ocp+[0 [headPos(1) -headPos(2)]./50]);
        % camva(4+2.*areaRatio);
        camlight(hlight,'headlight');
    tpf(tpfi,3)=toc;    % position adjustment (rendering)
        areaRingBuffer(areaRingIndex+1)=areaRatio;
        FILT = FILT + FF.*(areaRingBuffer-FILT); % use infinite impulse response (IIR) filter with filter fraction 1/4 
        areaRingIndex = mod(areaRingIndex+1,length(areaRingBuffer));
        % camva(4+2.*mean(FILT));
        % drawnow;
    else
        tpf(tpfi,[2 3])=NaN;
    end 
    tpf(tpfi,4)=toc;    % overall devision
    if(tpfi == minRuns)
        if ~einmalDurch
            disp('einmalDurch');
        end
        einmalDurch = true;
    end
    if(endlessMode)
        einmalDurch = false;
    end
end
set(hFig3D,'Position',[6   730   560   420]);
ind = any(isnan(tpf),2);disp(sum(ind))
disp(1000.*[mean(tpf(~ind,1)) mean(diff(tpf(~ind,:),[],2))])
disp(1000.*[std(tpf(~ind,1)) std(diff(tpf(~ind,:),[],2))])
disp(1000.*[mean(tpf(ind,1)) mean(diff(tpf(ind,:),[],2))])
disp(1000.*[std(tpf(ind,1)) std(diff(tpf(ind,:),[],2))])